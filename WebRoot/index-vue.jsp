<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%out.clear();
String clientCookie = request.getHeader("Cookie");
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>编辑器完整版实例-1.2.6.0</title>
    <link type="text/css" rel="Stylesheet" href="demo.css" />
    <link type="text/css" rel="Stylesheet" href="WordPaster/js/skygqbox.css" />
    <link href="umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="umeditor/third-party/jquery.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="umeditor/umeditor.config.js" ></script>
	<script type="text/javascript" src="umeditor/umeditor.min.js" ></script>
    <script type="text/javascript" src="WordPaster/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/skygqbox.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/w.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyCapture/z.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyOffice/js/o.js" charset="utf-8"></script>
    <script type="text/javascript" src="vue.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="demo.js" charset="utf-8"></script>
</head>
<body>
    <div id="demos"></div>    
    <div id="ue" style="width:100%;margin:20px auto 40px;">
        <ueditor ref="ue"></ueditor>
    </div>
    <script>
        Vue.component('ueditor', {
            data: function(){
                return {
                    editor: null
                }
            },
            props: {
                value: '',
                config: {}
            },
            mounted: function(){
            	var pos = window.location.href.lastIndexOf("/");
		        var api = [
		            window.location.href.substr(0, pos + 1),
		            "upload.jsp"
		        ].join("");
            	WordPaster.getInstance({
            		//上传接口：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
        	        PostUrl:api,
					//为图片地址增加域名：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
					ImageUrl: "",
					//设置文件字段名称：http://www.ncmem.com/doc/view.aspx?id=c3ad06c2ae31454cb418ceb2b8da7c45
					FileFieldName: "file",
					//提取图片地址：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
					ImageMatch: '',
        	        Cookie: '<%=clientCookie%>',
        	        event:{
						dataReady:function(e){
							//e.word,
							//e.imgs:tag1,tag2,tag3
							console.log(e.imgs)
						}
					}
        	    });//加载控件
		
                //zyCapture
                zyCapture.getInstance({
                    config: {
                        PostUrl: api,
                        FileFieldName: "file",
                        Fields: { uname: "test" }
                    }
                });

                //zyoffice，
                //使用前请在服务端部署zyoffice，
                //http://www.ncmem.com/doc/view.aspx?id=82170058de824b5c86e2e666e5be319c
                zyOffice.getInstance({
                    word:"http://localhost:13710/zyoffice/word/convert",
                    wordExport:"http://localhost:13710/zyoffice/word/export",
                    pdf:"http://localhost:13710/zyoffice/pdf/upload"
                });

                UM.getEditor('editor');
            },
            methods: {
                getUEContent: function(){
                    return this.editor.getContent()
                }
            },
            destroyed: function(){
                this.editor.destroy()
            },
            template: '<div><textarea id="editor" name="editor" style="width:100%;height:360px;"/></div>'
        });

        var ue = new Vue({
            el: '#ue',
            data: {
            }
            , mounted: function () {
            }
        });

    </script>
</body>
</html>