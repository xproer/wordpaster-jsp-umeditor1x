﻿/*
	版权所有 2009-2023 荆门泽优软件有限公司 保留所有版权。
	产品：http://www.ncmem.com/webapp/wordpaster/index.aspx
    控件：http://www.ncmem.com/webapp/wordpaster/pack.aspx
    示例：http://www.ncmem.com/webapp/wordpaster/versions.aspx
    版本：2.4.4
    时间：2023-07-12
    更新记录：
        2021-01-18 优化代码
		2012-07-04 增加对IE9的支持。
*/

function WordPasterManager()
{
    //url=>res/
    //http://localhost:8888/WordPaster/js/w.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.lastIndexOf("WordPaster/js/w.jqueryui.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("WordPaster/js/w.jqueryui.js"));
            }
        }
        return jsPath;
    };
    var rootDir = this.getJsDir()+"WordPaster/";//WordPaster
    //http://localhost/WordPaster/css/
    var pathRes = rootDir + "css/";

    var _this = this;
    UM.registerUI('wordpaster',
        function(name) {
            var me = this;
            var $btn = $.eduibutton({
                icon : name,
                click : function(){                    		
                    WordPaster.getInstance().SetEditor(me).Paste();
                },
                title: 'Word一键粘贴'
            });

            this.addListener('ready',function(){
                WordPaster.getInstance().SetEditor(me);
            });
            return $btn;
        }
    );
    UM.registerUI('importwordtoimg',
        function (name) {
            var me = this;
            var $btn = $.eduibutton({
                icon: name,
                click: function () {
                    WordPaster.getInstance().SetEditor(me).importWordToImg();
                },
                title: 'Word转图片'
            });

            this.addListener('ready', function () {
                WordPaster.getInstance().SetEditor(me);
            });
            return $btn;
        }
    );
    UM.registerUI('netpaster',
            function(name) {
                var me = this;
                var $btn = $.eduibutton({
                    icon : name,
                    click : function(){                    		
                        WordPaster.getInstance().SetEditor(me).UploadNetImg();
                    },
                    title: '自动上传网络图片'
                });

                this.addListener('ready',function(){
                    WordPaster.getInstance().SetEditor(me);
                });
                return $btn;
            }
    );
    UM.registerUI('wordimport',
        function (name) {
            var me = this;
            var $btn = $.eduibutton({
                icon: name,
                click: function () {
                    WordPaster.getInstance().SetEditor(me).importWord();
                },
                title: '导入Word文档'
            });

            this.addListener('ready', function () {
                WordPaster.getInstance().SetEditor(me);
            });
            return $btn;
        }
    );
    UM.registerUI('excelimport',
        function (name) {
            var me = this;
            var $btn = $.eduibutton({
                icon: name,
                click: function () {
                    WordPaster.getInstance().SetEditor(me).importExcel();
                },
                title: '导入Excel文档'
            });

            this.addListener('ready', function () {
                WordPaster.getInstance().SetEditor(me);
            });
            return $btn;
        }
    );
    UM.registerUI('pptimport',
        function (name) {
            var me = this;
            var $btn = $.eduibutton({
                icon: name,
                click: function () {
                    WordPaster.getInstance().SetEditor(me).importPPT();
                },
                title: '导入PPT文档'
            });

            this.addListener('ready', function () {
                WordPaster.getInstance().SetEditor(me);
            });
            return $btn;
        }
    );
    UM.registerUI('pdfimport',
        function (name) {
            var me = this;
            var $btn = $.eduibutton({
                icon: name,
                click: function () {
                    WordPaster.getInstance().SetEditor(me).ImportPDF();
                },
                title: '导入PDF文档'
            });

            this.addListener('ready', function () {
                WordPaster.getInstance().SetEditor(me);
            });
            return $btn;
        }
    );
    this.Editor = null;
    this.Fields = {}; //符加信息
    this.UploadDialogCreated = false;
    this.PasteDialogCreated = false;
    this.imgUploaderDlg = null;//jquery obj
    this.event={
        scriptReady: function () {
            $(function () {
                //加载
                if (typeof (_this.ui.render) == "undefined") {
                    _this.LoadTo($(document.body));
                }
                else if (typeof (_this.ui.render) == "string") {
                    _this.LoadTo($("#" + _this.ui.render));
                }
                else if (typeof (_this.ui.render) == "object") {
                    _this.LoadTo(_this.ui.render);
                }
            });
        },
        dataReady:function(e){
            //e.word
            //e.imgs
        }
    };
    this.ui = { setup: null,
        single: null,
        dialog: { files: false/**word图片上传窗口 */, paste: false/**粘贴窗口 */ },
        paste:{dlg:null,ico:null,msg:null,percent:null},
        ico: {
            error: pathRes + "error.png",
            upload: pathRes + "upload.gif"
        }
    };
    this.data={
        browser:{name:navigator.userAgent.toLowerCase(),ie:true,ie64:false,chrome:false,firefox:false,edge:false,arm64:false,mips64:false,platform:window.navigator.platform.toLowerCase()},
        error:{
        "0": "连接服务器错误",
        "1": "发送数据错误",
        "2": "接收数据错误",
        "3": "未设置文件路径",
        "4": "本地文件不存在",
        "5": "打开本地文件错误",
        "6": "不能读取本地文件",
        "7": "公司未授权",
        "8": "未设置IP",
        "9": "域名未授权",
        "10": "文件大小超出限制",
        "11": "不能设置回调函数",
        "12": "Native控件错误",
        "13": "Word图片数量超过限制"
      },
      type:{local:0/*本地图片*/,network:1/*网络图片*/,word:2/*word图片*/},
      scripts: [
          "css/w.css",
          "js/w.edge.js",
          "js/w.app.js",
          "js/w.file.js"
      ],
      jsCount: 0,//已经加载的脚本总数
    };
    
    this.loadScripts = function () {
        var head = document.getElementsByTagName('head')[0];
        //加载js
        for (var i = 0, l = this.data.scripts.length;
            i < l;
            ++i) {
            var n = this.data.scripts[i];
            if (-1 != n.lastIndexOf(".css")) {
                var css = document.createElement("link")
                css.setAttribute("rel", "stylesheet")
                css.setAttribute("type", "text/css")
                css.setAttribute("href", rootDir + n);
                head.appendChild(css);
            }
            else {
                this.requireJs(rootDir + n, function () {
                    _this.data.jsCount++;
                    if ((_this.data.jsCount + 1) == _this.data.scripts.length)
                        _this.event.scriptReady();
                });
            }
        }
    };
    this.requireJs = function (js, callback) {
        // create script element
        var head = document.getElementsByTagName('head')[0];

        var script = document.createElement("script");
        script.src = js;

        // monitor script loading
        // IE < 7, does not support onload
        if (callback) {
            script.onreadystatechange = function () {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    // no need to be notified again
                    script.onreadystatechange = null;
                    // notify user
                    callback();
                }
            };

            // other browsers
            script.onload = function () {
                callback();
            };
        }

        // append and execute script
        head.appendChild(script);
    };
    this.ffPaster = null;
    this.ieParser = null;
    this.ffPasterName = "ffPaster" + new Date().getTime();
    this.iePasterName = "iePaster" + new Date().getTime();
    this.setuped = false;//控件是否安装
    this.websocketInited = false;
    this.natInstalled = false;
    this.filesPanel = null;//jquery obj
    this.fileItem = null;//jquery obj
    this.line = null;//jquery obj
	this.Config = {
        "EncodeType"		    : "GB2312"
        , "Company"			    : "荆门泽优软件有限公司"
        , "Version"			    : "1,5,141,60875"
        , "License2"			: ""
        , "Debug"			    : false//调试模式
        , "LogFile"			    : "f:\\log.txt"//日志文件路径
        , "PasteWordType"	    : ""	//粘贴WORD的图片格式。JPG/PNG/GIF/BMP，推荐使用JPG格式，防止出现大图片。
        , "PasteImageType"	    : ""	//粘贴文件，剪帖板的图片格式，为空表示本地图片格式。JPG/PNG/GIF/BMP
        , "PasteImgSrc"		    : ""	//shape:优先使用源公式图片，img:使用word自动生成的图片
        , "JpgQuality"		    : "100"	//JPG质量。0~100
        , "PowerPoint"          : {sleep:500/*解析powerpoint时延迟时间*/}
        , "PDF"                 : {zoom:150/*缩放比例*/}
        , "QueueCount"		    : "5"	//同时上传线程数
        , "CryptoType"		    : "crc"//名称计算方式,md5,crc,sha1,uuid，其中uuid为随机名称
        , "ThumbWidth"		    : "0"	//缩略图宽度。0表示不使用缩略图
        , "ThumbHeight"		    : "0"	//缩略图高度。0表示不使用缩略图
        , "FileFieldName"		: "file"//自定义文件名称名称
        //图片地址匹配规则配置教程：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
        , "ImageMatch"		    : ""//服务器返回数据匹配模式，正则表达式，提取括号中的地址
        //自定义图片地址配置教程：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
        , "ImageUrl"		    : ""//自定义图片地址，格式"{url}"，{url}为固定变量，在此变量前后拼接图片路径，此变量的值为posturl返回的图片地址
        , "FileCountLimit"		: 300//图片数量限制
        , "AppPath"			    : ""
        , "Cookie"			    : ""
        , "Servers"             : [{"url":"www.ncmem.com"}]//内部服务器地址(不下载此地址中的图片)
        , "Proxy"               : {url: ""/**http://192.168.0.1:8888 */,pwd: ""/**admin:123456 */}//代理
        , "WebImg"              : {urlEncode:true/*下载外部图片地址是URL是否自动编码，默认情况下自动编码，部分网站URL没有进行编码*/}
        //上传接口配置教程：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
        , "PostUrl"			    : "http://www.ncmem.com/products/word-imagepaster/fckeditor2461/asp.net/upload.aspx"
        , "Fields"              : {}
        //x86
        ,ie:{name:"Xproer.WordParser2",clsid:"2404399F-F06B-477F-B407-B8A5385D2C5E",path:"http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster.cab"}
        ,ie64:{name:"Xproer.WordParser2x64",clsid:"7C3DBFA4-DDE6-438A-BEEA-74920D90764B",path:"http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster64.cab"}
        //Firefox
        , "XpiType"	            : "application/npWordPaster2"
        , "XpiPath"		        : "http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster.xpi"
        //Chrome
        , "CrxName"		        : "npWordPaster2"
        , "CrxType"	            : "application/npWordPaster2"
        , "CrxPath"		        : "http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster.crx"
        //Edge
        , edge: { protocol: "wordpaster", port: 17092, visible: false }
        , "ExePath": "http://res2.ncmem.com/download/WordPaster/pdf/1.0.23/WordPaster.exe"
        , "mac": { path: "http://res2.ncmem.com/download/WordPaster/mac/1.0.36/WordPaster.pkg" }
        , "linux": { path: "http://res2.ncmem.com/download/WordPaster/linux/1.0.25/com.ncmem.wordpaster_2020.12.3-1_amd64.deb" }
        , "arm64": { path: "http://res2.ncmem.com/download/WordPaster/arm64/1.0.22/com.ncmem.wordpaster_2020.12.3-1_arm64.deb" }
        , "mips64": { path: "http://res2.ncmem.com/download/WordPaster/mips64/1.0.19/com.ncmem.wordpaster_2020.12.3-1_mips64el.deb" }
    };

    if (arguments.length > 0) {
        var cfg = arguments[0];
        if (typeof (cfg) != "undefined") $.extend(true, this.Config, cfg);
        if (typeof (cfg.ui) != "undefined") $.extend(true, this.ui, cfg.ui);
        if (typeof (cfg.event) != "undefined") $.extend(true, this.event, cfg.event);
    }

	this.EditorContent = ""; //编辑器内容。当图片上传完后需要更新此变量值
	this.CurrentUploader = null; //当前上传项。
	this.UploaderList = new Object(); //上传项列表
    //已上传图片列表
    //模型：LocalUrl:ServerUrl
	this.UploaderListCount = 0; //上传项总数
	this.fileMap = new Object();//文件映射表。
	this.postType = this.data.type.word;//默认是word
    this.working = false;//正在上传中
    this.pluginInited = false;
    var browserName = navigator.userAgent.toLowerCase();
	this.data.browser.ie = this.data.browser.name.indexOf("msie") > 0;
    //IE11
	this.data.browser.ie = this.data.browser.ie ? this.data.browser.ie : this.data.browser.name.search(/(msie\s|trident.*rv:)([\w.]+)/) != -1;
	this.data.browser.firefox = this.data.browser.name.indexOf("firefox") > 0;
    this.data.browser.chrome = this.data.browser.name.indexOf("chrome") > 0;    
	this.data.browser.chrome45 = false;
    this.data.browser.edge = this.data.browser.name.indexOf("Edge") > 0;
    this.data.browser.arm64 = this.data.browser.platform.indexOf("aarch64")>0;
    this.data.browser.mips64 = this.data.browser.platform.indexOf("mips64")>0;
	this.chrVer = navigator.appVersion.match(/Chrome\/(\d+)/);
	this.ffVer = this.data.browser.name.match(/Firefox\/(\d+)/);
	if (this.data.browser.edge) { this.data.browser.ie = this.data.browser.firefox = this.data.browser.chrome = this.data.browser.chrome45 = false; }

    this.initApp = function(){
        this.edgeApp = new WebServer(this);
        this.edgeApp.ent.on_close = function () { _this.pluginInited=false; };
        this.app = WordPasterApp;
        this.app.ins = this;
    };
    this.checkBrowser=function(){

        //Win64
        if (window.navigator.platform == "Win64")
        {
            $.extend(this.Config.ie,this.Config.ie64);
        }
        else if (this.data.browser.ie) {

        }//macOS
        else if (window.navigator.platform == "MacIntel") {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.mac.path;
        }
        else if (window.navigator.platform == "Linux x86_64") {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.linux.path;
        }
        else if (this.data.browser.arm64) {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.arm64.path;
        }
        else if (this.data.browser.mips64) {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.mips64.path;
        }//Firefox
        else if (this.data.browser.firefox)
        {
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.data.browser.edge = true;
        } //chrome
        else if (this.data.browser.chrome)
        {
            _this.Config["XpiPath"] = _this.Config["CrxPath"];
            _this.Config["XpiType"] = _this.Config["CrxType"];

            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
        }
        else if (this.data.browser.edge)
        {
            this.app.postMessage = this.app.postMessageEdge;
        }
    };

    this.pluginLoad = function () {
        if (!this.pluginInited) {
            if (this.data.browser.edge) {
                this.edgeApp.connect();
            }
        }
    };
    this.pluginCheck = function () {
        if (!this.pluginInited) {
            this.setup_tip();
            this.pluginLoad();
            return false;
        }
        return true;
    };
    this.setup_tip = function () {
        this.ui.dialog.paste = true;
        this.ui.setup.dialog({
            width: 410
            , height: 151
            , modal: true
            , close: function (e, ui) {
                _this.ui.dialog.paste = false;
            }
        });
        var dom = this.ui.setup.html("<div>控件加载中，如果未加载成功请先</div><span name='setup' class='btn'><img name='setup'/>安装控件</span><span name='setupOk' class='btn'><img name='ok'/>我已安装</span>");
        dom.find('span[name="setup"]').click(function () {
                window.open(_this.Config["ExePath"]);
            });
        dom.find(".btn").each(function () {
            $(this).hover(function () {
                $(this).addClass("btn-hover");
            }, function () {
                $(this).removeClass("btn-hover");
            });
        });
        dom.find('span[name="setupOk"]').click(function () {
            _this.pluginLoad();
        });
        dom.find("img[name='ok']").attr("src", pathRes + "ok.png");
        dom.find("img[name='setup']").attr("src", pathRes + "setup.png");
    };
    this.need_update = function ()
    {
        this.ui.dialog.paste = true;
        this.ui.setup.dialog({
            width: 170
            , height: 113
            , close: function (e, ui) {
                _this.ui.dialog.paste = false;
            }
        });
        var dom = this.ui.setup.html("发现新版本，请<a name='w-exe' href='#' class='btn'>更新</a>");
        var lnk = dom.find('a[name="w-exe"]');
        lnk.attr("href", this.Config["ExePath"]);
    };
	this.setupTipClose = function ()
	{
	    var dom = this.ui.paste.msg.html("图片上传中......");
	    this.ui.paste.percent.show();
	    this.ui.paste.ico.show();
        this.CloseDialogPaste();
        this.setuped = true;
	};
	this.CheckUpdate = function ()
	{
	    if (this.Browser.CheckVer())
	    {
	        this.OpenDialogPaste();
	        var dom = this.ui.paste.msg.html("控件有新版本，请<a name='aCtl'>更新控件</a>");
	        var lnk = dom.find('a[name="aCtl"]');
	        lnk.attr("href", this.Config["ExePath"]);
	        this.ui.paste.percent.hide();
	        this.ui.paste.ico.hide();
	    }
	};

    //加载控件及HTML元素
	this.GetHtml = function ()
	{
	    //Word图片粘贴
	    var acx = "";
        //Word解析组件
        acx += ' <object name="' + this.iePasterName + '" classid="clsid:' + this.Config.ie.clsid + '"';
	    acx += ' codebase="' + this.Config.ie.path + '#version=' + this.Config["Version"] + '"';
	    acx += ' width="1" height="1" ></object>';
	    if (this.data.browser.edge) acx = '';
	    //单张图片上传窗口
	    acx += '<div name="imgPasterDlg" class="panel-paster" style="display:none;">';
	    acx += '<img name="ico" id="infIco" alt="进度图标"/><span name="msg">图片上传中...</span><span name="percent">10%</span>';
        acx += '</div>';
        //安装提示
        acx += '<div name="ui-setup" class="panel-paster panel-setup"></div>';
	    //图片批量上传窗口
        acx += '<div name="filesPanel" class="panel-files" style="display: none;"></div>';
	    //
	    acx += '<div style="display: none;">';

	    //文件上传列表项模板
	    acx += '<div class="UploaderItem" name="fileItem" id="UploaderTemplate">\
		            <div class="UploaderItemLeft">\
		            <div name="fname" class="FileName top-space">HttpUploader程序开发.pdf</div>\
		            <div class="ProcessBorder top-space">\
		                <div name="process" class="Process"></div>\
		            </div>\
		            <div name="msg" class="PostInf top-space">已上传:15.3MB 速度:20KB/S 剩余时间:10:02:00</div>\
		        </div>\
		        <div class="UploaderItemRight">\
		            <a name="btn" class="Btn" href="javascript:void(0)">取消</a>\
		            <div name="percent" class="ProcessNum">35%</div>\
		        </div>';
	    acx += '</div>'; //template end
	    //分隔线
	    acx += '<div name="line" class="Line" id="FilePostLine"></div>';

	    //hide div end
	    acx += '</div>';
	    return acx;
	};

	this.LoadTo = function (o)
    {
        if (!WordPaster.inited)
        {
            var dom = o.append(this.GetHtml());
            this.ffPaster = dom.find('embed[name="' + this.ffPasterName + '"]').get(0);
            this.ieParser = dom.find('object[name="' + this.iePasterName + '"]').get(0);
            this.line = dom.find('div[name="line"]');
            this.fileItem = dom.find('div[name="fileItem"]');
            this.filesPanel = dom.find('div[name="filesPanel"]');
            this.imgUploaderDlg = dom.find('div[name="filesPanel"]');
            this.ui.paste.dlg = dom.find('div[name="imgPasterDlg"]');
            this.ui.single = dom.find('div[name="imgPasterDlg"]');
            this.ui.paste.ico = this.ui.paste.dlg.find('img[name="ico"]');
            this.ui.paste.ico.attr("src", this.ui.ico.upload);
            this.ui.paste.msg = this.ui.paste.dlg.find('span[name="msg"]');
            this.ui.paste.percent = this.ui.paste.dlg.find('span[name="percent"]');
            this.ui.setup = dom.find('div[name="ui-setup"]');

            this.init();
        }
        WordPaster.inited = true;
	};

    //在文档加载完毕后调用
	this.init = function ()
	{
        this.initApp();
        this.checkBrowser();
        if (!_this.data.browser.edge)
        {
            _this.parter = _this.ffPaster;
            if (_this.data.browser.ie) _this.parter = _this.ieParser;
            _this.parter.recvMessage = _this.recvMessage;
        }
        if (_this.data.browser.edge) {
            _this.edgeApp.connect();
        }
        else { _this.app.init(); }
	};

    //打开图片上传对话框
	this.OpenDialogFile = function ()
	{        
        if (!this.ui.dialog.files) {
            this.ui.dialog.files = true;
            _this.imgUploaderDlg.dialog({
                title: "Word图片上传"
                , width: 452
                , height: 445
                , close: function (e, ui) {
                    _this.ui.dialog.files = false;
                }
            });
        }
	};
	this.CloseDialogFile = function ()
	{
        _this.imgUploaderDlg.dialog('close');
        this.ui.dialog.files = false;
	};

    //打开粘贴图片对话框
	this.OpenDialogPaste = function ()
	{
        if (!this.ui.dialog.paste) {
            this.ui.dialog.paste = true;
            _this.ui.paste.dlg.dialog({
                title: ""
                , width: 550
                , height: 160
                , close: function (e, ui) {
                    _this.ui.dialog.paste = false;
                }
            });
        }
	};
	this.CloseDialogPaste = function ()
	{
        if (this.ui.dialog.paste)
        {
            this.ui.setup.dialog('close');
        }
        _this.ui.paste.dlg.dialog('close');
        this.ui.dialog.paste = false;
	};
	this.InsertHtml = function (html)
	{
	    _this.Editor.execCommand("insertHtml", html);
	};
	this.GetEditor = function () { return this.Editor; };

    //在FCKeditor_OnComplete()中调用
	this.SetEditor = function (edt)
	{
	    _this.Editor = edt;
        return this;
	};

    //粘贴命令
	this.Paste = function (evt)
	{
	    this.PasteManual();
	};

    //手动粘贴
	this.PasteManual = function ()
	{
	    if( !this.pluginCheck() ) return;
	    if( this.working ) return;
        this.working = true;
	this.app.paste();
	};

    //powerpoint
	this.PastePPT = function ()
	{
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.pastePPT();
    };

    //import word to img
    this.importWordToImg = function () {
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importWordToImg();
    };
    this.importWord = function(){
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importWord();        
    };
    this.importExcel = function(){
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importExcel();        
    };

    //powerpoint
	this.importPPT = function ()
	{
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importPPT();
	};

    //pdf
	this.ImportPDF = function ()
	{
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.app.importPDF();
	};

    //上传网络图片
	this.UploadNetImg = function ()
	{
		if( !this.pluginCheck() ) return;
	    var data = _this.Editor.getContent();
        this.app.pasteAuto(data);
	};

	/*
	根据ID删除上传任务
	参数:
	fileID
	*/
	this.Delete = function(fileID)
	{
		var obj = this.UploaderList[fileID];
		if (null == obj) return;

		var tbID = "item" + obj.FileID;
		var item = document.getElementById(tbID);
		if (item) document.removeChild(item); //删除
	};

	/*
		添加到上传列表
		参数
			index 上传对象唯一标识
			uploaderObj 上传对象
	*/
	this.AppendToUploaderList = function(index, uploaderObj)
	{
		this.UploaderList[index] = uploaderObj;
		++this.UploaderListCount;
	};

	/*
		添加到上传列表层
		参数
			fid 文件ID
			div 上传信息层对象
			obj 上传对象
	*/
	this.AppendToListDiv = function(fid, div, obj)
	{
		var line = this.line.clone(true); //分隔线
		line.attr("id", "FilePostLine" + fid)
            .css("display", "block");
		obj.Separator = line;

		this.filesPanel.append(div);
		this.filesPanel.append(line);
	};

	/*
		更新编辑器内容。
		在所有图片上传完后调用。
		在上传图片出现错误时调用。
	*/
	this.UpdateContent = function ()
	{
	    _this.InsertHtml(_this.EditorContent);
	};

	this.addImgLoc = function (img)
	{
	    var fid = img.id;
	    var task = new FileUploader(img.id, img.src, this, img.width, img.height);
	    this.fileMap[img.id] = task;//添加到文件映射表
	    var ui = this.fileItem.clone();
	    ui.css("display", "block").attr("id", "item" + fid);

	    var objFileName = ui.find('div[name="fname"]');
	    var divMsg = ui.find('div[name="msg"]');
	    var aBtn = ui.find('a[name="btn"]');
	    var divPercent = ui.find('div[name="percent"]');
	    var divProcess = ui.find('div[name="process"]');

	    objFileName.text(img.name).attr("title", img.name);
	    task.pProcess = divProcess;
	    task.pMsg = divMsg;
	    task.pMsg.text("");
	    task.pButton = aBtn;
	    aBtn.attr("fid", fid)
            .attr("domid", "item" + fid)
            .attr("lineid", "FilePostLine" + fid)
            .click(function ()
            {
                switch ($(this).text())
                {
                    case "暂停":
                    case "停止":
                        task.Stop();
                        break;
                    case "取消":
                        { task.Remove(); }
                        break;
                    case "续传":
                    case "重试":
                        task.Post();
                        break;
                }
            });
	    task.pPercent = divPercent;
	    task.pPercent.text("0%");
	    task.ImageTag = img.html; //图片标记
	    task.InfDiv = ui;//上传信息DIV

	    //添加到上传列表层
	    this.AppendToListDiv(fid, ui, task);

	    //添加到上传列表
	    this.AppendToUploaderList(fid, task);
	    task.Ready(); //准备
	    return task;
	};
	this.WordParser_PasteWord = function (json)
    {
	    this.postType = this.data.type.word;
	    this.EditorContent = json.word;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        this.addImgLoc(json.imgs[i]);
	    }
	};
	this.WordParser_PasteExcel = function (json)
	{
	    this.postType = this.data.type.word;
	    this.EditorContent = json.word;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        this.addImgLoc(json.imgs[i]);
	    }
	    this.OpenDialogFile();
	};
	this.WordParser_PasteHtml = function (json)
	{
	    this.postType = this.data.type.word;
	    this.InsertHtml(json.word);//
        this.working = false;
        this.CloseDialogFile();
	};
	this.WordParser_PasteFiles = function (json)
	{
	    this.postType = this.data.type.local;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        var task = this.addImgLoc(json.imgs[i]);
	        task.PostLocalFile = true;//
	    }
	    this.OpenDialogFile();
	};
	this.WordParser_PasteImage = function (json)
	{
	    this.OpenDialogPaste();
	    this.ui.paste.msg.text("开始上传");
	    this.ui.paste.percent.text("1%");
	};
    this.show_msg = function (msg) {
        this.OpenDialogPaste();
        this.ui.paste.msg.html(msg);
        this.ui.paste.percent.text("");
    };
	this.WordParser_PasteAuto = function (json)
	{
	    this.postType = this.data.type.network;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        this.addImgLoc(json.imgs[i]);
	    }
	    this.OpenDialogFile();
	};
	this.WordParser_PostComplete = function (json)
	{
	    this.ui.paste.percent.text("100%");
	    this.ui.paste.msg.text("上传完成");
	    var img = "<img src=\"";
	    img += json.value;
	    img += "\" />";
	    this.InsertHtml(img);
        this.working = false;
        this.CloseDialogPaste();
	};
	this.WordParser_PostProcess = function (json)
	{
	    this.ui.paste.percent.text(json.percent);
	};
	this.WordParser_PostError = function (json)
    {
        this.working = false;
		this.OpenDialogPaste();
        this.ui.paste.msg.html(
            this.data.error[json.value] + "<br/>" +
            "PostUrl:" + this.Config["PostUrl"] + "<br/>" +
            "License2:" + this.Config["License2"] + "<br/>" +
            "当前url:" + window.location.href + "<br/>" +
            "环境：" + navigator.userAgent);
        this.ui.paste.ico.attr("src", this.ui.ico.error);
	    this.ui.paste.percent.text("");
	};
	this.File_PostComplete = function (json)
	{
	    var up = this.fileMap[json.id];
	    up.postComplete(json);
	    delete up;//
	};
	this.File_PostProcess = function (json)
	{
	    var up = this.fileMap[json.id];
	    up.postProcess(json);
	};
	this.File_PostError = function (json)
	{
	    var up = this.fileMap[json.id];
	    up.postError(json);
	};
	this.Queue_Complete = function (json)
	{
	    //上传网络图片
	    if (_this.postType == this.data.type.network)
	    {
	        _this.GetEditor().setContent(json.word);
	    } //上传Word图片时才替换内容
	    else if (_this.postType == this.data.type.word)
	    {
	        _this.InsertHtml(json.word);//
            _this.event.dataReady(json);
	    }
        _this.working = false;
        this.CloseDialogFile();
	};
	this.load_complete_edge = function (json)
	{
		this.pluginInited = true;
        _this.app.init();
        this.CloseDialogPaste();
    };
    this.imgs_out_limit = function (json) {
        this.show_msg(this.data.error["13"] + "<br/>文档图片数量：" + json.imgCount + "<br/>限制数量：" + json.imgLimit);
    };
    this.url_unauth = function (json) {
        this.show_msg(this.data.error["9"] + "<br/>PostUrl：" + json.url);
    };
    this.state_change = function (json) {
        if (json.value == "parse_document")
        {
            this.OpenDialogFile();
            this.filesPanel.text("正在解析文档");
        }
        else if (json.value == "process_data") {
            this.filesPanel.text("正在处理数据");
        }
        else if (json.value == "export_image") {
            this.filesPanel.text("正在导出图片:"+json.index+"/"+json.count);
        }
        else if (json.value == "process_data_end")
        {
            this.filesPanel.text("");
        }
        else if (json.value == "parse_empty") {
            this.CloseDialogFile();
            _this.working = false;
        }
        else if (json.value == "feature_not_supported") {
            this.show_msg("此功能不支持");
            this.ui.paste.ico.attr("src",this.ui.ico.error);
            _this.working = false;
        }
    };
    this.load_complete = function (json)
    {
        if (this.websocketInited) return;
        this.websocketInited = true;
		this.pluginInited = true;

        var needUpdate = true;
        if (typeof (json.version) != "undefined")
        {
            this.setuped = true;
            if (json.version == this.Config.Version) {
                needUpdate = false;
            }
        }
        if (needUpdate) this.need_update();
    };
    this.recvMessage = function (msg)
	{
	    var json = JSON.parse(msg);
	    if      (json.name == "Parser_PasteWord") _this.WordParser_PasteWord(json);
	    else if (json.name == "Parser_PasteExcel") _this.WordParser_PasteExcel(json);
	    else if (json.name == "Parser_PasteHtml") _this.WordParser_PasteHtml(json);
	    else if (json.name == "Parser_PasteFiles") _this.WordParser_PasteFiles(json);
	    else if (json.name == "Parser_PasteImage") _this.WordParser_PasteImage(json);
	    else if (json.name == "Parser_PasteAuto") _this.WordParser_PasteAuto(json);
	    else if (json.name == "Parser_PostComplete") _this.WordParser_PostComplete(json);
	    else if (json.name == "Parser_PostProcess") _this.WordParser_PostProcess(json);
	    else if (json.name == "Parser_PostError") _this.WordParser_PostError(json);
	    else if (json.name == "File_PostProcess") _this.File_PostProcess(json);
	    else if (json.name == "File_PostComplete") _this.File_PostComplete(json);
	    else if (json.name == "File_PostError") _this.File_PostError(json);
        else if (json.name == "load_complete") _this.load_complete(json);
	    else if (json.name == "Queue_Complete") _this.Queue_Complete(json);
	    else if (json.name == "load_complete_edge") _this.load_complete_edge(json);
        else if (json.name == "state_change") _this.state_change(json);
        else if (json.name == "imgs_out_limit") _this.imgs_out_limit(json);
        else if (json.name == "url_unauth") _this.url_unauth(json);
    };

    this.loadScripts();
}

var WordPaster = {
    instance: null,
    inited:false,
    getInstance: function (cfg) {
        if (this.instance == null) {
            this.instance = new WordPasterManager(cfg);
            window.WordPaster = WordPaster;
        }
        return this.instance;
    }
}